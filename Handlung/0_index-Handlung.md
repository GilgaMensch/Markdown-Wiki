[Startseite](../Readme.md)

Handlung
==
#### [Szenen](Szenen/0_Szenenplan.md)
Hier sind ein [Szenenplan](Szenen/0_Szenenplan.md) zu finden, sowie die dazugehörigen Szenen.
#### [Szenenideen/Gesprächsfetzen](Szenenidee_Gespraechsfetzen/0_index-Ideefetzen.md)
Noch unzusammenhängende Ideen und Dialogstücke werden hier abgelegt.
