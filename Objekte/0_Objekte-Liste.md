[Startseite](../Readme.md)

Objekte
==
Objekte werden nach dieser [Vorlage](0_Template-Objekt.md) angelegt.

##### A
+ **AHP/E4-Sonde, *die*** (kurz für **Automated Harvest Probe / Extension Four**): Eine Sonde, die für den automatisierten Abbau von Rohstoffen auf vorbeifliegenden Asteroiden konzipiert ist; Hergestellt von *HTI*

##### B
+

##### C
+ **Civilization, *die*** (kurz **Civ**): Schiffsklasse, vom Typ *Kolonisationsschiff*; gefertigt von den amerikanischen Lunarwerften im Auftrag der *MKA*; Besonderheit ist der zweigeteilte Shiffskörper, ein Teil bildet eine Raumstation und der andere besteht aus Wohn-, Labor- und Technikmodulen, sowie einer Kuppel; insgesamt wurden sechs produziert; bekanntestes Beispiel ist die *Civ I*
+

##### D
+

##### E
+

##### F
+

##### G
+

##### H
+ **Himmelsdrache, *der***: chinesisches Raumschiff, welches als erstes ein Raumdock zum Mars brachte
+

##### I
+

##### J
+

##### K
+ **Kolonisationsschiff, *das***: ein Schiff welches genug Material und Besatzung trägt, um eine dauerhafte Niederlassung auf einem anderen Planeten zu etablieren
+ **Kuppel, *die***: Ein annähernd halbkugelförmiger Bau, welcher großteils verglast ist und über strahlensichere mechanische Schotts verfügt; innerhalb der Kuppeln finden sich Strahler, welche die Wellenlänge des Sonnenlichts abgeben, die der des Erdenlichts entsprechen, sowie Grünanlagen zur Sauerstoffaufbereitung, wie Nahrungsbereitstellung; Auch befriedigt ein solcher Bau den menschlichen Wunsch den Himmel sehen zu können und den tunnelartigen Modulen zu entfliehen, in denen einem in Stresssituationen der Himmel auf den Kopf fällt
+

##### L
+ **Legacy-Klasse, *die***: Schiffsklasse, welche zur einen Hälfte aus *Atmosphären-Konvertern* und zur anderen aus Kuppel-Bauteilen besteht;
+

##### M
+

##### N
+

##### O
+

##### P
+

##### Q
+

##### R
+

##### S
+

##### T
+

##### U
+

##### V
+

##### W
+

##### X
+

##### Y
+

##### Z
+
