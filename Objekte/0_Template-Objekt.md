[Liste der Objekte](0_Objekte-Liste.md) | [Startseite](../Readme.md)

Objekt
==
![Default Logo](../Assets/0_Skizze-default.png)
## Gerelles
* **Name/Bezeichnung**:
* **Art d. Objektes**:
* **Beschreibung**:

## Aussehen
* **Material**:
* **Gewicht**:
* **Physisches Aussehen**:

## Besitzverhältnisse
* **Erster Besitzer**:
* **vorherige Besitzer**:
* **Momentaner Besitzer**:
* **Erschaffer d. Objekts**:

## Geschichte
* **Jahr der Erschaffung**:
* **Wofür**:
* **bisherige Geschichte**:
* **momentane Geschichte**:
* **zukünftige Geschichte**?:

## Fähigkeit/Brauchbarkeit
* **Benutzung**:
* **Spezielles**:
