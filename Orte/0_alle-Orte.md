[Startseite](../Readme.md)

Orte
==
Die Orte werden in zwei Listen organisiert. In [historische](0_Orte-Liste_historische.md) und [handlungstragende](0_Orte-Liste_handlungstragende.md). Es kann auch zu Überschneidungen kommen.
Die Orte sind nach dieser [Vorlage](0_Template-Ort.md) strukturiert.
