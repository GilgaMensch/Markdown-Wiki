[Alle Orte](0_alle-Orte.md) | [historische](0_Orte-historische.md) | [handlungstragende](0_Orte-handlungstragende.md) | [Startseite](../Readme.md)

Name
==
![Default Logo](../Assets/0_Skizze-default.png)![Default Logo](../Assets/0_Logo-default.png)
* **Abkürzung**:

## Wetter
* **Winter**:
* **Frühling**:
* **Sommer**:
* **Herbst**:

## Monument
* **Auffälliges**:

## Wahrnehmung
* **Sehen**:
* **Riechen**:
* **Hören**:
* **Schmecken**:
* **Fühlen**:

## Kurzbeschreibung
Lorem ipsum

## Handlungsrelevanz
* **Szene**:
