###### [Assets](Assets/0_index.md) | [Charaktere](Charaktere/0_Personen-Liste.md) | [Personen](Personen/0_Personen-Liste.md) | [Ereignisse](Ereignisse/0_Event-Liste.md) | [Handlung](Handlung/0_index.md) | [Organisationen](Organisationen/0_Organisationen-Liste.md) | [Objekte](Objekte/0_Objekte-Liste.md) | [Orte](Orte/0_alle-Orte.md) | [Nationen](Welt/0_Nationen-Liste.md) | [Anderes](Other/0_index.md)

Startseite
==
Auf dieser Seite ist eine Übersicht über die verschiedenen Unterkategorien dieses Projektes. So findet sich hier auch ein übergreifendes [Glossar](Glossar.md) und einen [Handlungsansatz](Outline.md)

## Inhaltsverzeichnis

### [Assets](Assets/0_index.md)
Der Ordner Assets beeinhaltet alle Bild-, Ton- und sonstigen Dateien, die nicht als Text dargestellt werden können.

### [Charaktere](Charaktere/0_Personen-Liste.md)
Im Ordner Charaktere sind sämtliche der handlungstragenden Personen hinterlegt. (nicht zu verwechseln mit [Personen](Readme.md#Personen))

### [Ereignisse](Ereignisse/0_Event-Liste.md)
Hier sind die einschneidenden Ereignisse dieser Welt zu finden.

### [Handlung](Handlung/0_index-Handlung.md)
#### [Szenen](Handlung/Szenen/0_Szenenplan.md)
Hier sind ein [Szenenplan](Handlung/Szenen/0_Szenenplan.md) zu finden, sowie die dazugehörigen Szenen.

#### [Szenenideen/Gesprächsfetzen](Handlung/Szenenidee_Gespraechsfetzen/0_uebersicht-Ideen.md)
Noch unzusammenhängende Ideen und Dialogstücke werden hier abgelegt.

### [Objekte](Objekte/0_Objekte-Liste.md)
Objekte, die in irgendeinerweise in der Handlung auftauchen oder Erklärungsbedürftig sind, sind hier zu finden.

### [Organisationen](Organisationen/0_Organisationen-Liste.md)
In der Geschichte/Welt tauchen immer mal wieder Firmen/Organisationen/Religionen auf. Die sind hier aufgelistet.

### [Orte](Orte/0_alle-Orte.md)
Es tauchen in der Handlung/Welt immer mal wieder [historische](Orte/0_Orte-Liste_historische.md) und [handlungstragende](Orte/0_Orte-Liste_handlungstragende.md) Orte auf, die in den jeweiligen Listen aufgelistet sind und zu Teil in einem eigenen Artikel representiert werden.

### [Personen](Personen/0_Personen-Liste.md)
Hier sind die Personen aufgelistet, die in der Geschichte dieser Welt eine größere Rolle getragen haben oder das komplette Gegenteil sind und nur in einem Nebensatz von anderen Charakteren erwähnt werden.

### [Nationen](Welt/0_Nationen-Liste.md)
Hier sind die verschiedenen Länder und Nationen der Welt aufgelistet, sowie die [Grundlagen](Welt/0_Welt.md) der Welt.

### [Anderes](Other/0_index.md)
Hier befindet sich alles, was nocht nicht zugeordnet wurde.

[Zum Anfang](Readme.md)
