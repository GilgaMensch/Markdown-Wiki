[Startseite](../Readme.md)

Handlungstragende Personen
==

In dieser Liste sind alle Charaktere aufgelistet, die in der Handlung eine Rolle spielen.
Die weiterführenden Charakterbögen basieren auf dieser [Vorlage](0_Template-Charakterbögen.md).

##### A
+

##### B
+

##### C
+

##### D
+

##### E
+

##### F
+

##### G
+

##### H
+

##### I
+

##### J
+

##### K
+

##### L
+

##### M
+

##### N
+

##### O
+

##### P
+

##### Q
+

##### R
+

##### S
+ 

##### T
+

##### U
+

##### V
+

##### W
+

##### X
+

##### Y
+

##### Z
+
