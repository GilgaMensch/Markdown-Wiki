[Charakterverzeichnis](0_Personen-Liste.md) | [Startseite](../Readme.md)

Charaktereigenschaften
==
![Default Logo](../Assets/0_Skizze-default.png)
## Basic
**Name**:|[hier eintragen]
---|-
Namensherkunft/-bedeutung:|
**Spitzname**:|
Herkunft/Bedeutung/Anekdote:|
**Alter**:|
Alterserscheinung:|
**Geschlecht**:|
**Geburtstag**:|
Ereignis am Geburtstag:|
**Geburtort**:|
spezieller Ort?:|
**Ethnik/Spezies**:|
spezielles:|
**Beruf/Berufung**:|
Stellung:|
**Welt/Universum/Planet**:|
Kurzbeschreibung:|
**Charakter in einem Satz**:|

## Auftreten
**Größe**: |[1,70m]
---|--
**Gewicht**:|
Körperform:|
**Hauton**:|
**Gesichtsform**:|
**Augenfarbe**:|
**ins Auge springende Merkmal**:|
**weitere Merkmale**:|
**Frisur**:|
Haarfarbe:|
**Körperform**:|
**Haltung**:|
**Kleidungsstil**:|
**Was ist immer dabei?**(Waffe/Werkzeug):|
**Accesoires**:|
**weiteres zum Aussehen**:|

## Leben
Bezeichnung|Beschreibung
---|-
**Fähigkeiten**:|[hier eintragen]
**Inkompetenz**:|
**Stärken/Talente**:|
**Schwächen**:|
**Hobbies**:|
**Gewohnheiten**:|
**(Haus-)Tiere**:|
**Aufenthaltsort/Umgebung**:|
**Beschreibung Zuhause**:|
**Nachbarschaft**:|
**Organisation beteiligt**:|
**Einkommen/Auskommen durch**:|
**Zufriedenheit im Beruf**:|
**Gesundheit**:|

## Verhalten
Bezeichnung|Beschreibung
---|-
**Persönlichkeit**:|
**Moral/Ethik**:|
**Selbstbeherrschung**:|
**Motivation**:|
**Was zieht ihn/sie/es runter**:|
**Intelligenz**:|
**Selbstbewusstsein**:|
**Philosophie**:|
**größte Furcht/Phobie**:|

## Vergangenheit
Bezeichnung|Beschreibung
---|-
**Kindheit**:|
**Einschlägige Ereignisse**:|
**größte Errungenschaft**:|
**weitere Errungenschaften**:|
**schlechtestes Erlebnis**:|
**Fehler**:|
**Geheimnis**:|
**schönste Errinerung**:|
**schlimmste Erinnerung**:|

## Handlungsbezug
Bezeichnung|Beschreibung
---|-
**Rolle in der Geschichte**:|
**Ausrichtung**:|
**Ziele auf kurze Sicht**:|
**Ziele auf lange Sicht**:|
**erstes Auftreten**:|
**Handlungsstrang/-beteiligung**:|
**Konflikt**:|
**Charakterdefinierender Moment**:|

## Beziehungen

## Weiteres
