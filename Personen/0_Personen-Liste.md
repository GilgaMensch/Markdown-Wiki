[Startseite](../Readme.md)

Historische Personen
==
Hier sind Personen aufgelistet, die in der Handlung keine tragende Rolle besitzen oder nur in einem Nebensatz erwähnt werden.
Die Personen sind [hiernach](0_Template-Personen.md) strukturiert.
##### A
+

##### B
+

##### C
+

##### D
+

##### E
+

##### F
+

##### G
+

##### H
+

##### I
+

##### J
+

##### K
+

##### L
+

##### M
+

##### N
+

##### O
+

##### P
+

##### Q
+

##### R
+

##### S
+

##### T
+

##### U
+

##### V
+

##### W
+

##### X
+

##### Y
+

##### Z
+
