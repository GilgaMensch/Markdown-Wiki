[Liste der Nationen](0_Nationen-Liste.md) | [Welt](0_Welt.md) | [Startseite](../Readme.md)

Nation_A
==
![Default Logo](../Assets/0_Flagge-default.png)![Default Logo](../Assets/0_Umriss-default.png)
## Basic
* **Name**:
* **Überblick**:
* **Geschichtsabriss**:

## Kultur
* **Religionen/Glauben**:
* **Ethik und Grundsätze**:
* **Sprachen**:
* **Klassen-/Kastensystem**:

## System
* **Technologie**:
* **Wirtschaft**:
* **Währung**:
* **Handel**:
* **Kommunikation**:
* **Kunst und Unterhaltung**:
* **Reisen**:
* **Wissenschaft und Entdeckungen**:
* **Unternehmen/Industrie**:

## Politik
* **Regierungsform**
* **Gesetze und Justiz**:
* **Regierungshistorie**:
* **Außenpolitik**:
* **Wahrnehmung in der Öffentlichkeit**:
* **Propaganda**:
* **Gegenbewegungen**:
* **Krieg**:
