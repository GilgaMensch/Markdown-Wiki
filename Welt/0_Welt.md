[Liste der Nationen](0_Nationen-Liste.md) |  [Startseite](../Readme.md)


Welt
==
## Basic
* **Name**:
* **Überblick**:
* **Erdähnlich/alternative Erde/nicht Erde**:
* **Geschichte**:

## Biologie
* **Rassen**:
* **Pflanzen**:
* **Tiere und Kreaturen**:
* **natürliche Ressourcen**:
* **Wetter**:

## Nationen
* **Nation_A**:

## Kultur
* **Religionen/Glauben**:
* **Ethik und Grundsätze**:
* **Sprachen**:
* **Klassen-/Kastensystem**:

## System
* **Technologie**:
* **Wirtschaft**:
* **Währung**:
* **Handel**:
* **Kommunikation**:
* **Reisen**:
* **Wissenschaft und Entdeckungen**:
* **Unternehmen/Industrie**:

## Magie
* **Magiearten**:
* **Magieregeln**:
* **Vorraussetzungen**:
* **Effekte auf die Welt**:
* **Effekte aud die Gesellschaft**:
* **Effekte auf die Technologie**:
