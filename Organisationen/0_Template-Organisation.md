[Liste von Organisationen](0_Organisationen-Liste.md) | [Startseite](../Readme.md)

[Organisation]
==
![Default Logo](../Assets/0_Logo-default.png)
* **Kürzel**:
* **Gründung**:
* **Kurzbeschreibung**:
* **Branche**:
* **Umsatz**
* **Ziele und Tätigkeitsschwerpunkte**:

## Schlüsselpersonen
* **Gründer**:
* **Gesicht der Organisation**:
* **Geschäftsführer**:
* **weitere Posten**:
  * [hier eintragen]:

## Beschreibung
[hier eintragen]

## Ziele
* **Politische**:
* **Wirtschaftliche**:
* **Nach außen**:

## Auftreten
* **Gegenüber der Öffentlichkeit**:
* **Gegenüber den Mitarbeiter**:

## Geschichte
[hier eintragen]

## Organisationsstruktur
* **Niederlassungen**:
