<head>
  <link rel="stylesheet" href="github-markdown.css">
  <link rel="stylesheet" href="style.css">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
</head>
<?php require_once('Parsedown.php'); #Parsedown Bibliothek laden?>

<body>
<div id="header" class="header">
  <h1><img src="Assets/MW_Logo.png" alt="Markdown Wiki Logo" id="Logo"> Markdown Wiki</h1>
</div>

<div id="content-pane" class="markdown-body">

<?php
// Nächstes Dokument bestimen
$self = $_SERVER['PHP_SELF'];
$FALLBACK ="Readme.md";
if (empty($_GET["page"])){
  $file = $FALLBACK;
} else {
  $file = $_GET["page"];
}

// Hole Inhalt aktuellen Dokumentes
$content = file_get_contents($file);
$content_change = $content;

// Adresse aktuellen Dokumentes
$dir_current = explode("/",$file);

// Documentlinks finden
if(preg_match_all("/\]\(.*?\.md\)/", $content, $matches))
{
  // console_log("Anzahl Dokumentlinks: ".count($matches[0]));
  foreach ($matches[0] as $match)
  {
    // Identifier entfernen
    $match = ltrim($match, "](");
    $match = rtrim($match, ")");

    $dir_cc = $dir_current;
    $dirs = explode("/",$match);

    // Für alle Dokumente, die nicht in ein höheres Verzeichnis verweisen
    if (!(strcmp($dirs[0], "..")==0))
    {
      console_log("untergeordneter Link: ".$match);
      $dir_cc[count($dir_cc)-1] = implode("/",$dirs);
      $ziel = "$self?page=".implode("/",$dir_cc);

      if(!strstr($content_change,$ziel)==$ziel)
      {
        $content_change = str_replace(implode("/", $dirs), $ziel, $content_change);
      }
    } else
    // Für alle Dokumente, die auf eine Ordnerstruktur verweisen, welche hierarchisch höher liegt
    {
      // console_log("übergeordneter Link: ".$match);
      $folders_back = 0;
      while(strcmp($dirs[$folders_back], "..")==0)$folders_back++;
      // console_log("Anzahl übergeordneter Ordner: ".$folders_back);

      $dir_cc = array_slice($dir_cc, count($dir_cc)-$folders_back-2, count($dir_cc));
      $dir_cc[count($dir_cc)-1] = implode("/",array_slice($dirs,$folders_back));
      $ziel = "$self?page=".implode("/",$dir_cc);

      // console_log("übergeordneter Link: ".$match);
      if(!strstr($content_change,$ziel)==$ziel)
      {
        $content_change = str_replace(implode("/", $dirs), $ziel, $content_change);
      }
    }
  }
}

// Hier werden die Referenzlinks der Bilddateien verändert
if(preg_match_all("/!\[.*\]\(.*\)/", $content, $matches))
{
  console_log("Anzahl Bilddateien: ".count($matches[0]));
  foreach ($matches[0] as $match)
  {
    $match = explode("(",$match)[1];
    $match = rtrim($match, ")");
    // console_log("Bilddatei: ".$match);

    $dir_cc = $dir_current;
    $dirs = explode("/",$match);


    // Für alle Dokumente, die nicht in ein höheres Verzeichnis verweisen
    if (!(strcmp($dirs[0], "..")==0))
    {
      $dir_cc[count($dir_cc)-1] = implode("/",$dirs);
      // $ziel = explode("/", $self)[1]."/".implode("/",$dir_cc);

      $ziel = implode("/",$dir_cc);
      // console_log("Neuer Ziellink: ".$ziel);

      // Überprüfung, ob bereits ersetzt
      if(!strstr($content_change,$ziel)==$ziel)
      {
        $content_change = str_replace(implode("/", $dirs), $ziel, $content_change);
      }
    } else
    // Für alle Bilder, die auf eine Ordnerstruktur verweisen, welche hierarchisch höher liegt
    {
      // console_log("übergeordnete Bildlinks: ".$match);
      $folders_back = 0;
      while(strcmp($dirs[$folders_back], "..")==0) $folders_back++;

      // Neue Adresse erzeugen
      $dir_cc = array_slice($dir_cc, count($dir_cc)-$folders_back-2, count($dir_cc)-1);
      $dir_cc[count($dir_cc)-1] = implode("/",array_slice($dirs,$folders_back));
      $ziel = implode("/",$dir_cc);
      // console_log("Neuer Ziellink: ".$ziel);

      // Überprüfung, ob bereits ersetzt
      if(!strstr($content_change,$ziel)==$ziel)
      {
        $content_change = str_replace(implode("/", $dirs), $ziel, $content_change);
      }
    }
  }
}

// $content_change = str_replace("|\n|","|", $content_change);

// Parsen des Markdown-Dokumentes
$Parsedown = new Parsedown();
$parsed = $Parsedown->text($content_change);

// Ausgeben des Dokumentes, nach Überprüfung auf 'Linkrelikte'
echo str_replace("../","",$parsed);
// echo $parsed;


// Funktion zur Ausgabe auf der Konsole
 function console_log( $data ) {
     $output = $data;
     if ( is_array( $output ) )
         $output = implode( ',', $output);
     echo "<script>console.log( 'Debug: " . $output . "' );</script>";
 }


// For Same-Folder-Documents
// if(count($dirs)==1)
// {
//   // printf("<br>Match: ".$match);
//   // printf("<br>Bevor: ".implode("/",$dir_cc));
//
//   $dir_cc[count($dir_cc)-1] = $dirs[0];
//   $ziel = "$self?page=".implode("/",$dir_cc);
//
//   // printf("<br>Danach: $ziel<br>");
//
//   if(!strstr($content_change,$ziel)==$ziel)
//   {
//     $content_change = str_replace(implode("/", $dirs), $ziel, $content_change);
//   }
// } else
?>
</div>
</body>
